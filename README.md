# Time Tracking - Front End

this is the front-end for the Time Tracking Simple Project.
please make sure the server is up first.

and than run those command 

    npm install
    npm start

### Pros:
![alt text](./public/assets/images/screenshot.png)

* Clean and Nice UI. built from Zero
* Never Used any third-party for building UI (but ofcourse some bootstrap css). so you can be sure i can use React to handle different things
* User Can Show/Add records. and also delete, restore 
* Clean Reuseable Component to show Alerts
* Responsive 


### Cons:
* Not Using Redux
* i sacrifice some best practicing points due to time (as i said i'm working for a company and office. i haven't the enough time)





