import React, { Component } from 'react';

import './App.css';
import SideBar from './Components/ui/sidebar/SideBar';
import TimeTracking from './Components/ui/TimeTracking/TimeTracking';



class App extends Component {
  constructor() {
    super();
    this.state = {
      render: 0,
    }
  }
  
  reload() {
   
    this.setState({
      render: this.state.render + 1
    });
  }

  render() {
    return (
      <div className="App">
        <div className="row">
          <div className="col-md-3 side-div">
            <SideBar render={this.state.render} />
          </div>
          <div className="col-md-9 main-div">
            <TimeTracking reload={this.reload.bind(this)} />
          </div>
        </div>
        
       
      </div>
    );
  }

  componentWillMount() {

  }
}

export default App;
