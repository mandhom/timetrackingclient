import React, { Component } from 'react';
import TimeTrackingList from '../../comp/TimeTrackingList/TimeTrackingList'
import Alert from '../../comp/Alert/Alert';

import axios from 'axios'

class SideBar extends Component {
  serverAddr = "http://localhost:9000/";
  renderVer = 0;
 

  constructor() {
    super();
    this.state = {
      listItems: [],
      search: "",
      messages: [],
      currentPage: 1,
      deleteStatus: 0,
    };

  }

  selectItem(item) {
    let NewArray = this.state.listItems.map((dbitem, i) => {
      if (item.ID === dbitem.ID) {
        if (dbitem.Active === true) {
          dbitem.Active = false;
        } else {
          dbitem.Active = true;
        }
        
      } else {
        dbitem.Active = false;
      }
      return dbitem;
    });

    this.setState({
      listItems: NewArray
    })
  }

  deleteItem(item) {
    axios.delete(this.serverAddr + item.ID + "/archive", {params: {ID: item.ID}}).then((response) => {
      if (response.data.Code === "") {

        this.setState({
          messages: [{message: "ok! record has deleted.", class: "alert-success"}],
        });

        this.getTimeTrackingLog();
      }
    });
  }

  
  showDeletedChange() {
    let newDeleteStatus = 0;
    if (this.refs.deletedstatus.checked) {
      newDeleteStatus = 2;
    } 


    this.setState({
      deleteStatus:newDeleteStatus
    }, () => {
      this.getTimeTrackingLog();
    });
  }

  getDeletedStatus() {
    if (this.refs.deletedstatus && this.refs.deletedstatus.checked) {
      return true;
    }
    return false;
  }

  unDeleteItem(item) {
    axios.delete(this.serverAddr + item.ID + "/unarchive", {params: {ID: item.ID}}).then((response) => {
      if (response.data.Code === "") {
        this.setState({
          messages: [{message: "ok! record has restored.", class: "alert-success"}],
        }, () => {
          this.getTimeTrackingLog();
        });
      }
    });
  }
  
  searchChange(e) {
    this.setState({
      search: e.target.value ,
    }, () => {
      this.getTimeTrackingLog();
    });
  }

  clearMessage(){
    this.setState({
      messages: []
    });
  }

  loadNext() {
    if (this.state.listItems.length !== 10) {
      return;
    }
    this.setState({
      currentPage: this.state.currentPage + 1
    }, () => {
      this.getTimeTrackingLog();
    });
  }
  
  loadPrev() {
    if (this.state.currentPage <= 1) {
      return;
    }
    this.setState({
      currentPage: this.state.currentPage - 1
    }, () => {
      this.getTimeTrackingLog();
    });
  }
  render() {
    return (
      <div className="sidebar">
        <div>
          <h1> What you have done </h1>
            <div className="global-form timeform">
                    <div className="form-group">
                        <input ref="searchinput" className="form-control search" placeholder="Search"  onChange={this.searchChange.bind(this)} />
                    </div>

                    <div className="form-check form-check-inline">
                      <input className="form-check-input" type="checkbox" id="inlineCheckbox1" ref="deletedstatus" onChange={this.showDeletedChange.bind(this)}/> 
                      <label className="form-check-label" htmlFor="inlineCheckbox1">View Deleted Only</label>
                    </div>
                    <div className="form-group">
                      <span className="btn" onClick={this.loadPrev.bind(this)}>  ‹‹ Prev </span>
                      ||
                      <span className="btn" onClick={this.loadNext.bind(this)}> Next ›› </span>
                    </div>
                   

            </div> 
          </div>
         
            <TimeTrackingList 
            list={this.state.listItems} 
            onSelectItem={this.selectItem.bind(this)} 
            onDeleteItem={this.deleteItem.bind(this)}
            onUnDeleteItem={this.unDeleteItem.bind(this)}
            isInDelete={this.getDeletedStatus()}
            />
       

        <Alert 
      messages={this.state.messages} 
      onClose={this.clearMessage.bind(this)}
      />
      </div>
    );
  }

  componentWillMount() {
    
  }

  getTimeTrackingLog(deletedStatus = 0, searchText = "") {
    console.log("update");
    axios.get(this.serverAddr, {
      params: {
        delete: this.state.deleteStatus,
        search: this.state.search,
        page: this.state.currentPage
      }
    }).then((response) => {
      if (response.data.Code === "") {
        this.setState({
          listItems: response.data.Data
        });
  
      }
    })
  }

  componentDidMount() {
    this.getTimeTrackingLog();
  }
  componentWillUpdate() {
    //
  }
  shouldComponentUpdate(nextProps, nextStats){
    return true;
  }
  componentWillReceiveProps(propm) {
   
    if (this.renderVer !== propm.render) {
      this.getTimeTrackingLog();
    }
  
  }
}

export default SideBar;
