import React, { Component } from 'react';
import TimeTrackingAddModel from '../../comp/TimeTrackingAddModel/TimeTrackingAddModel';
import Alert from '../../comp/Alert/Alert';

import axios from 'axios'

class TimeTracking extends Component {
  interval;
  serverAddr = "http://localhost:9000/";

  constructor() {
    super();
    this.state = {
      duration: 0,
      isTimerOn: false, 
      isOpenAddForm: false,
      timelogs: [],
      pauseButtonClass: "btn global-button global-button--blue start-button none",
      startButtonClass: "btn global-button global-button--blue puse-button block",
      messages: [],

    }
  }

  
  
  startTimer() {
    this.setState({
      isTimerOn: true, 
      pauseButtonClass: "btn global-button global-button--blue start-button",
      startButtonClass:"btn global-button global-button--blue start-button none"
    });
  }

  pauseTimer() {
    this.setState({
      isTimerOn: false, 
      pauseButtonClass: "btn global-button global-button--blue start-button none",
      startButtonClass:"btn global-button global-button--blue start-button"
    });
  }

  openBookMark() {
    this.setState({
      isTimerOn: false, 
      pauseButtonClass: "btn global-button global-button--blue start-button none",
      startButtonClass:"btn global-button global-button--blue start-button",
      isOpenAddForm: true, 
    });
  }

  onCloseForm() {
    this.setState({
      isOpenAddForm: false, 
    });
  }
  
  getDuration() {
    return new Date(this.state.duration * 1000).toISOString().substr(11, 8)
  }

  submit(trackData) {

    axios.post(this.serverAddr, trackData).then((response) => {
      if (response.data.Code === "") {
 
        this.setState({
          messages: [{message: "Wow! Good Job.", class: "alert-success"}],
          duration: 0,
          isOpenAddForm: false,
      
        });
 
        this.props.reload();
      
      } else {
        console.log(response.data)
        this.setState({
          messages: [{message: "Oh! Something Went Wrong.", class: "alert-danger"}],
          duration: 0,
          isOpenAddForm: false,
  
        });
      }
     
    });

  

  }

  getBookmarkClass() {
    if (this.state.duration > 0) {
      return "";
    }
    return " disabled";
  }
  
  setTimerInt() {
    this.interval = setInterval(() => {
      if (this.state.isTimerOn) {
        this.setState({
          duration: this.state.duration + 1
        })
      }
    }, 1000);
  }
  clearMessage(){
    this.setState({
      messages: []
    });
  }

  render() {
    return (
      <div>
        <div className="main-box">
          <h1 className="big-title duration-box"> {this.getDuration()} </h1>
          <p> This Simple Demo WebApp Help You Track Your Time. and What you have done in simple and clean way. just click on Start, to start tracking </p>
          <button href="#" className={this.state.startButtonClass} onClick={this.startTimer.bind(this)}> <i className="arrow-right v-text-top"></i> Start </button>
          <button href="#" className={this.state.pauseButtonClass} onClick={this.pauseTimer.bind(this)}> <i className="pause-icon v-text-top"></i> Pause </button>
          <button href="#" className={"btn global-button global-button--orange bookmark-button" } onClick={this.openBookMark.bind(this)}  > <i className="bookmark-icon v-text-top"></i>Bookmark </button>
        </div>
        <TimeTrackingAddModel 
        isOpen={this.state.isOpenAddForm} 
        whenCloseForm={this.onCloseForm.bind(this)} 
        whenSubmit={this.submit.bind(this)}
        duration={this.state.duration} 
        />

      <Alert 
      messages={this.state.messages} 
      onClose={this.clearMessage.bind(this)}
      />
      </div>
      );
  }

  componentWillMount() {
    
  }

  componentDidMount() {
    this.setTimerInt();
  }

  
  componentWillUnmount() {
    clearInterval(this.interval);
  }
}

export default TimeTracking;
