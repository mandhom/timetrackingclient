import React, { Component } from 'react';



class Alert extends Component {

  
  renderAlerts() {
  
    if (this.props.messages) {
      return this.props.messages.map((item, index) => {
        return (
          <div key={index} className={item.class ? "alert "+item.class : "alert alert-danger"} onClick={this.props.onClose.bind(this)}>
             {item.message ? item.message : "No Message"}
          </div>
        )
      });
    }

   
  }

  render() {
    return (
     <div className="alert-box">
      {this.renderAlerts()}
    </div>
    );
  }

  
  componentWillUpdate() {
    if ( this.props.messages) {

     
      this.props.messages.map((item, index) => {
        if (!item.message) {
          item.message = "No Message";
        }
        if (!item.class) {
          item.class = "alert-danger";
        }
        return item;
      });

    
    }
  }

  componentDidUpdate() {
    
  }
}

export default Alert;
