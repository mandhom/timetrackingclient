import React, { Component } from 'react';



class TimeTrackingItem extends Component {



   months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
   colors = ["--red", "--blue", "--green", "--orange"];

  getDuration() {
      let durationString = new Date(this.props.item.Duration * 1000).toISOString().substr(11, 8);
      durationString = durationString.replace(":", " Hours ")
      durationString = durationString.replace(":", " Minutes ")
      durationString += " Seconds"
      return durationString;
  }

  selectItem() {
    this.props.onSelectItem();
  }

  getActive() {
      if (this.props.item.Active) {
          return " active";
      }
      return "";
  }

  getDate() {
    let date = new Date(this.props.item.AddUnix*1000);
    let year = date.getFullYear();
    let month = date.getMonth();
    let month_string =  this.months[month];
    let day = date.getDate();
    let hours = date.getHours();
    let minutes = "0" + date.getMinutes();
    return day + " " + month_string + " " + year + " - " + hours + ":" + minutes
  }

  getColorClass() {
   
    return this.colors[this.props.item.Color - 1];
  }

  deleteItem() {
      this.props.onDeleteItem();
  }
  unDeleteItem() {
   
      this.props.onUnDeleteItem();
  }
  renderDeleteOrUnDelete() {
      if (this.props.isInDelete) {
        return <span className="text-danger delete-time-item" onClick={this.unDeleteItem.bind(this)}> Restore </span>

      }
      return <span className="text-danger delete-time-item" onClick={this.deleteItem.bind(this)}> X </span>
  }

  
  render() {
    return (
      <div className={"time-item" + this.getActive()} >
        <h3> {this.getDate()}  </h3>
        <div className="time-item-content">
          <div className={"color-op color-box color-op" + this.getColorClass()} ></div>
          <div className="time-item-content-main">
            <h3> {this.props.item.Title} </h3>
            <p> {this.getDuration()} </p>
            
            <div className="time-item-content-details">
                {this.props.item.Details} 
            </div>


            <a className={"show-hide-button text" + this.getColorClass()} onClick={this.selectItem.bind(this)}> <span> Hide </span> Details </a>
        
          </div>

          {this.renderDeleteOrUnDelete()}

        </div>
      </div>
    );
  }

  componentWillMount() {

  }
}

export default TimeTrackingItem;
