import React, { Component } from 'react';
import TimeTrackingItem from '../TimeTrackingItem/TimeTrackingItem';

class TimeTrackingList extends Component {

  selectItem(item) {
    this.props.onSelectItem(item)
  }

  deleteItem(item) {
    this.props.onDeleteItem(item);
  }

  unDeleteItem(item) {
      this.props.onUnDeleteItem(item);
  }
  renderItems() {
      
      if (this.props.list) {

        return this.props.list.map((item, index)=> {
            return <TimeTrackingItem 
            item={item} key={index} 
            onSelectItem={this.selectItem.bind(this, item)} 
            onDeleteItem={this.deleteItem.bind(this, item)}
            isInDelete={this.props.isInDelete}
            onUnDeleteItem={this.unDeleteItem.bind(this, item)}
            />
        });


      } 
  }

  render() {
    return (
     <div className="time-list">
        {this.renderItems()}
      </div>
    );
  }

  componentWillMount() {

  }
}

export default TimeTrackingList;
