import React, { Component } from 'react';


class TimeTrackingAddModel extends Component {
  constructor() {
    super();
    this.state = {
        colors: [],
    }
 
  }

  getVisibleStatusClass() {
      if (this.props.isOpen) {
          return "";
      }
      return " none";
  }

  closeModal () {
    this.props.whenCloseForm();
  }


  submit() {
    let duration = this.props.duration;
    if (this.refs.duration.value !== "" && this.props.duration === 0) {
        duration = parseInt(this.refs.duration.value);
    }

    
      let trackerData = {
          Title: this.refs.title.value,
          Details: this.refs.details.value,
          Color: this.getSelectedColorVal(),
          Duration: duration
      }

     
      this.props.whenSubmit(trackerData);
  }
  
  selectColor(index) {
    let newColorArray = this.state.colors.map((item, i) => {
        if (index === i) {
            item.active = true;
        } else {
            item.active = false;
        }
        
        return item;
    });

 
   this.setState({
       colors: newColorArray
   })
  }

  renderColors() {
    return this.state.colors.map((item, index) => {
        return (
        <div className={item.active ? item.class + " active" : item.class} key={index} onClick={this.selectColor.bind(this, index)} >

        </div>
            );
    });
  }

  getSelectedColorVal(){
      let selectedVal;
      this.state.colors.map((item, i) => {
          if (item.active === true) {
             selectedVal = item.val;
          }
          return;
      });

      return selectedVal;
  }

  getManualDurationClass() {
      if (this.props.duration > 0) {
        return  "form-group none"
      } else {
        return "form-group"
      }
  }
  render() {
    return (
      <div className={"global-model " + this.getVisibleStatusClass()}>
            <div className="global-close" onClick={this.closeModal.bind(this)}></div>
            <div className="global-model-content">
                <h2>What have you done</h2>
                <div className="global-form timeform">

                    <div className={ this.getManualDurationClass() }>
                        <input ref="duration" type="number" className="form-control" placeholder="Duration in Seconds" />
                    </div>

                    <div className="form-group">
                        <input ref="title" className="form-control" placeholder="Title of the achievement" />
                    </div>
                    <div className="form-group">
                        <textarea ref="details" className="form-control" rows="3" placeholder="Description of the achievement"></textarea>
                    </div>

                    <p> Choose a Color </p>
                    <div className="color-options">
                        {this.renderColors()}
                    </div>
                </div>

                <button href="#" className="btn global-button global-button--green" onClick={this.submit.bind(this)}> <i className="mark-icon v-text-top"></i> Save </button>
            </div>
      </div>
    );
  }

  componentWillMount() {
      this.setState({
          colors: [
              {class: "color-op color-op--red", active: false, val: 1},
              {class: "color-op color-op--blue", active: true, val: 2},
              {class: "color-op color-op--green", active: false, val: 3},
              {class: "color-op color-op--orange", active: false, val: 4}

          ]
      })
  }


}

export default TimeTrackingAddModel;
